.386						; Compile for a 80386 CPU
 option segment:use16				; Force 16 bit segments instead of default 32 bit
.model tiny					; Tiny memory model
.code						; Start of code segment
 org 07c00h					; Bootloader entry point



;---------------------------------------------------
main:
jmp start
nop
OSPrompt db "MarkOS>",0
input equ 7e00h

help_str db "help",0
clear_str db "clear",0
beep_str db "beep",0
theme_str db "theme ",0
echo_str db "echo ",0

include stdio.h
include stdlib.h
include string.h
;
; Summary:  Start of the main operating system code.
;
;---------------------------------------------------
start:
    cli
    mov ax, 0				; Set AX to zero
    mov ds, ax					; Set data segment to zero
    mov es, ax
    add ax, 20h					; Skip over the size of the bootloader divided by 16 (512 / 16)
    mov ss, ax					; Set segment register to current location (start of the stack)
    mov sp, 4096				; Set ss:sp to the top of the 4k stack
    sti
    ;ss = 32
    ;sp = 4096
    ;ds = 0
main_loop:
    mov esi,OFFSET OSPrompt ;print prompt
    call OutString

    mov esi,OFFSET input ;get input string
    mov ecx,65
    call InString

    call Newline

    mov ecx,OFFSET help_str ;check if input contains 'help'
    mov edx,OFFSET input
    call StrCont
    cmp al,1
    je Help

    mov ecx,OFFSET clear_str ;check if input contains 'clear'
    call StrCont
    cmp al,1
    je Clear

    mov ecx,OFFSET beep_str ;check if input contains 'beep'
    call StrCont
    cmp al,1
    je Beep

    mov ecx,OFFSET echo_str ;check if input contains 'echo'
    call StrCont
    cmp al,1
    je print

    mov ecx,OFFSET theme_str ;check if input contains 'theme'
    call StrCont
    cmp al,1
    je Theme


    jmp main_loop

    Help:
        mov esi,OFFSET help_str ;print 'help'
        call OutString
        call Newline

        mov esi,OFFSET clear_str ;print 'clear'
        call OutString
        call Newline

        mov esi,OFFSET beep_str ;print 'beep'
        call OutString
        call Newline

        mov esi,OFFSET theme_str ;print 'theme XX'
        call OutString
        mov al,'X'
        call OutChar
        call OutChar
        call Newline

        mov esi,OFFSET echo_str ;print 'echo X'
        call OutString
        mov al,'X'
        call OutChar
        call Newline
        jmp main_loop

    Clear:
        mov ax,0003h ; use interrupt to clear screen
        int 10h
        jmp main_loop

    Beep:
        mov al,7 ;print the bell char to make a sound
        call OutChar
        jmp main_loop

    print:
        
        mov esi,OFFSET input ;echo the argument the user types after echo
        add esi,5
        call OutString
        call Newline
        jmp main_loop       

    Theme:
        mov esi,OFFSET input ;move the command pointer to esi
        add esi,5 ;increment the pointer past the command (now points to argument)
        call StrtoHex ;convert arg to hex
        mov bh,al ;move the user's argument to bh
        mov ah, 06h  
        mov al,0 
        mov cx,0 
        mov dx, 184FH
        int 10H ;set screen bg and fg color to value in bh
        mov ah,02 
        mov bh,0
        mov dx,0
        int 10h ;set cursor to top of screen
        jmp main_loop

    byte 510 - ($ - main) dup (0)		; Pad remainder of boot sector with zeros
    dw 0aa55h					; Boot signature
END main

