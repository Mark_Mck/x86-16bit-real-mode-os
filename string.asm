;-------------------------------------------------
; StrCont: determine if one string is contained within another
; Inputs:
; ecx: pointer to sub-string to search for
; edx: pointer to string to search within for substring
; Outputs:
; al: 1 if substring in string; else 0
;--------------------------------------------------

StrCont:
mov ebx,0
StrContLoop:
mov al, byte PTR [ecx+ebx] ; move current char of substring to al
mov ah, byte PTR [edx+ebx] ; move current char of string to ah
cmp byte ptr[ecx+ebx],0 ;check if at end of substring
je StrCmpTrue ;if strcont reaches the end of substring, it is in string
cmp al,ah ;compare substring char to string char
jne StrCmpFalse
inc ebx ;increment pointer
jmp StrContLoop
StrCmpTrue: ;return true
    mov al,1
    ret
StrCmpFalse: ;return false
    mov al,0
    ret