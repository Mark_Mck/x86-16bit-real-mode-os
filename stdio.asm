
;----------------------------------------------
; StrtoHex: Converts an ascii string to its hexadecimal value
; Inputs:
; esi: pointer to string to convert
; Outputs:
; eax: hex value of the input string
;----------------------------------------------
StrtoHex:
    mov ebx,0 ;set the sum to 0
    mov eax,0 ;clear eax
StrtoHexLoop:
    mov al,byte ptr [esi] ;move the current char of the string to al
    cmp al,0 ;check if @ end of string
    je StrtoHexRet
    xchg eax,ebx ;ebx now contains the current char, eax contains the sum
    mov cl,16
    mul cl ;multiply the value by 16
    xchg eax,ebx ;switch the values back
    call ChartoHex ;convert the current char to a digit
    add ebx,eax ;add the value of the digit to the sum
    inc esi ;increment the string pointer
    jmp StrtoHexLoop
StrtoHexRet:
    mov eax,ebx
    ret

;-------------------------------------------------
; InString: reads in a string from the keyboard buffer and stores it at a given memory address
; Inputs:
; ecx: the maximum number of chars to read in
; esi: a pointer to the string destination
; Outputs:
; eax: length of the string entered
;------------------------------------------------
InString:
    mov edx,1 ;reset dx (char counter), save room for null terminator
InStringLoop:
    mov ah,00h 
    int 16h ;get a char from the keyboard in al
    cmp al,08h ;check if the char is backspace
    je InStringBckSpc
    cmp al,0dh ;check if char is enter
    je InStringTerminate
    cmp cx,dx ;check if the number of chars it equal to the max number of chars
    je InStringLoop
    mov [esi+edx-1],al ;move the current char to the current point in the dest string
    mov ah, 0eh 
    int 10h;print the character
    inc dx ;increment the number of chars
    jmp InStringLoop
InStringBckSpc:
    cmp dx,1 ;check is the current number of chars entered is 0
    jg InStringBckSpc2 
    jmp InStringLoop
InStringBckSpc2:
    dec dx ;decrement the number of chars
    mov byte ptr [esi+edx-1],0 ;move a null char to the current pointer value
    mov al,8 
    mov ah, 0eh 
    int 10h ;move cursor back one space
    mov al, ' ' 
    int 10h ; print a blank space
    mov al, 8
    int 10h ;move the cursor back again
    jmp InStringLoop
InStringTerminate:
    dec dx 
    mov byte ptr [esi+edx],0 ;null terminate the string
    mov eax,edx ;move number of chars to eax
    ret

;----------------------------------------------
; OutString: print a string
; Inputs:
; esi: pointer to string to print
; Outputs:
; None
;---------------------------------------------
OutString:
    lodsb ;mov current char to al
    or al,al ;chech is current char is null terminator
    jz OutStringRet
    call OutChar ;print the character
    jmp OutString
OutStringRet:
    ret

;----------------------------------------------
; OutChar: print a char
; Inputs:
; al: char to print
; Outputs:
; None
;-----------------------------------------------
OutChar:
    mov ah, 0eh
    int 10h 
    ret
