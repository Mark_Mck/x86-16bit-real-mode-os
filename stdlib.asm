;-------------------------------------------
; ChartoHex: converts an ascii character to its hex value
; Inputs:
; al: char to convert
; Outputs:
; al: hex value of char
;----------------------------------------------
ChartoHex: 
	cmp al,'A' ;check if char is upper case
	jl	ChartoHexInt
    cmp al,'a' ;check if char is lower case
    jl ChartoHexUprAlpha
	jmp ChartoHexLwrAlpha
ChartoHexInt:
	sub al,48 ;subtract 48 from ascii value for upper case char
	ret
ChartoHexLwrAlpha:
    sub al,87 ;subtract 87 from ascii value for lower case char
    ret
ChartoHexUprAlpha:
	sub al,55 ;subtract 55 from value of interger
	ret

;----------------------------------------------
; NewLine: Print a newline
; Inputs:
; None
; Outputs:
; None
;-----------------------------------------------
Newline:
    push eax ;push eax to stack
    mov al,13 
    call OutChar ;print carriage return
    mov al,10
    call OutChar ;print line-feed
    pop eax
    ret
